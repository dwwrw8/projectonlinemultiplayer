extends Node2D

var current_lobby_data

# Called when the node enters the scene tree for the first time.
func _ready():
	NetworkManager.connect("new_lobby_data", self, "update_lobby_data")
	
	NetworkManager.request_lobby_data()

func update_lobby_data(lobby_data):
	
	$PlayersInLobby.text = "Players in Lobby: " + str(len(lobby_data[NetworkManager.currentLobby]["players"].keys()))
	
	if get_tree().get_network_unique_id() != lobby_data[NetworkManager.currentLobby]["owner"]:
		$Button2.disabled = true
	else:
		$Button2.disabled = false
	
	for child in $ScrollContainer/HFlowContainer.get_children():
		child.queue_free()
		
	var scene = load("res://Scenes/playerlisting.tscn")

	for playerID in lobby_data[NetworkManager.currentLobby]["players"].keys():
		var my_data = lobby_data[NetworkManager.currentLobby]["players"][playerID]
		var scene_instance = scene.instance()
		scene_instance.set_name(my_data["name"])
		$ScrollContainer/HFlowContainer.add_child(scene_instance)
		scene_instance.updateData(lobby_data, playerID)
		
		current_lobby_data = lobby_data

func on_back_button_pressed():
	NetworkManager.request_leave_lobby()
	get_tree().change_scene("res://Scenes/LobbyList.tscn")

func on_start_button_pressed():
	NetworkManager.request_start_game(NetworkManager.currentLobby)
	
