extends Control

onready var scoreLabel = $ScoreLabel

export var TimerGradient:Gradient

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func update_scores(players:Dictionary, player_scores:Dictionary):
	var scoretexts = []
	for id in player_scores.keys():
		scoretexts.append([player_scores[id], "%s: %s\n" % [players[id]["name"], stepify(player_scores[id], 0.01)]])
	scoretexts.sort_custom(MyCustomSorter, 'sort_ascending')
	var text = ""
	for entry in scoretexts.slice(0,10):
		text += entry[1]
	scoreLabel.text = text

func show_timer():
	$LevelEndTimer.show()

func hide_timer():
	$LevelEndTimer.hide()

func update_time(percent:float) -> void:
	$LevelEndTimer.value = percent
	$LevelEndTimer.self_modulate = TimerGradient.interpolate(percent)

class MyCustomSorter:
	static func sort_ascending(a, b):
		if a[0] < b[0]:
			return true
		return false
