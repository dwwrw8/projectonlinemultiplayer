#NetworkManager.gd
#This is autoloaded as a singleton.
extends Node

#const DEFAULT_IP = "localhost"		# local
const DEFAULT_IP = "64.15.81.244"	# michael
#const DEFAULT_IP = "3.15.188.170"	# amazon server
const DEFAULT_PORT = 26969
const DEFAULT_MAX_PLAYERS = 200
const SERVER_ID = 1
const DEFAULT_CONNECTION_TYPE = "client"

signal new_lobby_data(lobby_data)
signal acknowledge_lobby_created()
signal acknowledge_lobby_join()

var lobby_data := {}
var currentLobby = ""
var my_data := {
	'name': 'Anonymous', 
	'color': Color.white,
	"sprite": 'normal',
	"trail": 'normal',
	"trail_color": Color.orangered,
}

var result_players
var result_scores
var result_times
var result_gamemode


func new_player_name(name):
	my_data['name'] = name


#Request, Send, Receive chain for lobby data, ie list of lobbies and each player in each lobby.
func request_lobby_data():
	rpc_id(SERVER_ID, "send_lobby_data")

master func send_lobby_data():
	rpc_id(get_tree().get_rpc_sender_id(), "receive_lobby_data", lobby_data)

puppet func receive_lobby_data(new_lobby_data):
	lobby_data = new_lobby_data
	emit_signal("new_lobby_data", lobby_data)

func update_lobby_data():
	rpc("receive_lobby_data", lobby_data)


#Request and acknowledge chain for creating a lobby.
func request_create_lobby(lobby_name):
	rpc_id(SERVER_ID, "server_create_lobby", lobby_name, get_tree().get_network_unique_id())

master func server_create_lobby(lobby_name, ownerID):
	var lobbyID = create_lobby(lobby_name, ownerID)
	rpc_id(get_tree().get_rpc_sender_id(), "acknowledge_lobby_created", lobbyID)

puppet func acknowledge_lobby_created(lobbyID):
	currentLobby = lobbyID
	rpc_id(SERVER_ID, "add_player", lobbyID, my_data)
	emit_signal("acknowledge_lobby_created")


func request_join_lobby(lobbyID):
	currentLobby = lobbyID
	rpc_id(SERVER_ID, "add_player", lobbyID, my_data)
	emit_signal("acknowledge_lobby_join")


func request_leave_lobby():
	var myID = get_tree().get_network_unique_id()
	rpc_id(SERVER_ID, "remove_player", currentLobby, myID)
	currentLobby = ""

#Server functions for creating/removing a lobby and player.
func create_lobby(lobby_name, ownerID):
	assert(is_network_master())
	var newID = UUID.NewID()
	var lobby := {
		"name": lobby_name,
		"started": false,
		"owner": ownerID,
		"players": {}
	}
	lobby_data[newID] = lobby
	update_lobby_data()
	return newID

mastersync func remove_lobby(lobbyID):
	lobby_data.erase(lobbyID)
	update_lobby_data()

mastersync func add_player(lobbyID, player_data):
	lobby_data[lobbyID]["players"][get_tree().get_rpc_sender_id()] = player_data
	update_lobby_data()

mastersync func remove_player(lobbyID, playerID):
	lobby_data[lobbyID]["players"].erase(playerID)
	if lobby_data[lobbyID]["players"].empty():
		remove_lobby(lobbyID)
	elif lobby_data[lobbyID]["owner"] == playerID:
		var random_id = randi() % lobby_data[lobbyID]["players"].size()
		lobby_data[lobbyID]["owner"] = lobby_data[lobbyID]["players"].keys()[random_id]
	update_lobby_data()


	
func request_start_game(lobbyID):
	rpc_id(SERVER_ID, "startGame", lobbyID)

master func startGame(lobbyID):
	lobby_data[lobbyID]["started"] = true
	update_lobby_data()
	GameService.start_game(lobby_data[lobbyID]["players"])

func startOfflineGame():
	GameService.start_offline_game({0: my_data})


# Start a server from this instance
func create_server(server_port : int = DEFAULT_PORT):
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(server_port, DEFAULT_MAX_PLAYERS)
	get_tree().set_network_peer(peer)
	#players[SERVER_ID] = my_data

# Connect to an external server
func connect_to_server(server_ip : String = DEFAULT_IP, server_port: int = DEFAULT_PORT):
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(server_ip, server_port)
	get_tree().set_network_peer(peer)

func get_player_count():
	return get_tree().get_network_connected_peers().size()

func get_my_name():
	return my_data["name"]

# This just connects signals for later use
func _ready():
	# Network signals.
# warning-ignore:return_value_discarded
	get_tree().connect("connection_failed", self, "_connected_fail")
# warning-ignore:return_value_discarded
	get_tree().connect('connected_to_server', self, '_on_connected_to_server')
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_connected', self, '_on_player_connected')
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')

func _connected_fail():
	Notifications.notify("Connection Failed")

# Called by client, tells the server to add our data
func _on_connected_to_server():
	Notifications.notify("Connected to server")
	#rpc_id(1, "register_player", my_data)

func _on_player_connected(other_player_id):
	Notifications.notify("Player %s Connected" % str(other_player_id))

func _on_player_disconnected(playerID):
	# Make sure only the network master runs this
	if not is_network_master(): return
		
	for game in GameService.get_children():
		if playerID in game.players.keys():
			game.on_player_left(playerID)
			break
	
	var lobbyID
	
	for lobby in lobby_data.keys():
		if playerID in lobby_data[lobby]["players"].keys():
			lobbyID = lobby
	
	if lobbyID != null:
		remove_player(lobbyID, playerID)
	
	Notifications.notify("Player %s Disonnected" % str(playerID))
