extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	
	randomize()
	
	var args = parse_os_args()
	match args.get('network-connection-type', NetworkManager.DEFAULT_CONNECTION_TYPE):
		"server":
# warning-ignore:return_value_discarded
			NetworkManager.create_server()
		"client", _:
# warning-ignore:return_value_discarded
			get_tree().change_scene("res://Scenes/Menu.tscn")

# godot-server -package "mygame.pck" --network-connection-type="server"

# Read args from cmd into dict
func parse_os_args():
	var arguments = {}
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			arguments[key_value[0].lstrip("--")] = key_value[1]
	
	return arguments
