extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	NetworkManager.connect("new_lobby_data", self, "update_lobby_data")
	NetworkManager.connect("acknowledge_lobby_created", self, "on_lobby_created")
	NetworkManager.connect("acknowledge_lobby_join", self, "on_lobby_joined")
	
	NetworkManager.request_lobby_data()

func _process(_delta):
	$PlayersOnline.text = "Players Online: " + str(NetworkManager.get_player_count())

func update_lobby_data(lobby_data):
	for child in $ScrollContainer/VBoxContainer.get_children():
		child.queue_free()
		
	var scene = load("res://Scenes/lobbylisting.tscn")
	
	for id in lobby_data.keys():
		if lobby_data[id]["started"] == false:
			var scene_instance = scene.instance()
			scene_instance.set_name(id)
			$ScrollContainer/VBoxContainer.add_child(scene_instance)
			scene_instance.updateData(lobby_data, id)
			scene_instance.connect("JoinButton_pressed", self, "on_join_lobby_pressed")
	
	print(lobby_data)

func on_create_lobby_pressed():
	#make prompt for name appear
	var scene = load("res://Scenes/lobbyname.tscn")
	var scene_instance = scene.instance()
	add_child(scene_instance)
#	print("waiting for input")
	scene_instance.connect("lobby_name_entered", self, "on_lobby_name_entered")
	#NetworkManager.request_create_lobby()

func on_lobby_name_entered(lobby_name):
	if lobby_name.empty(): 
		lobby_name = "Lobby"
	NetworkManager.request_create_lobby(lobby_name)

func on_join_lobby_pressed(lobbyID):
#	print("I have joined")
	NetworkManager.request_join_lobby(lobbyID)

func on_lobby_created():
	get_tree().change_scene("res://Scenes/playerlist.tscn")

func on_lobby_joined():
	get_tree().change_scene("res://Scenes/playerlist.tscn")

func on_back_button_pressed():
	get_tree().network_peer = null
	get_tree().change_scene("res://Scenes/ConnectionMenu.tscn")
