extends Viewport

var PORT = 0
var MAX_PLAYERS = 8
var SERVER_ID = 1

var PLAYER_SCENE = preload("res://Scenes/Subscenes/Player.tscn")

var players = {}
var players_loaded = []
var players_finished = []
var player_scores = {}
var player_times = {}
var level_sequence = {}
var cur_level

var USE_END_TIMER := true
var END_TIMER_DUR := 45.0 #seconds
var is_end_timer_active := false
var end_timer_value = 0.0 # current value of end timer

onready var camera = $Camera
onready var UI = $UILayer/UI

var timer_started = false

var spawn_point = Vector2.ZERO

# Send an RPC to every player in this game. Called by player object.
func rpc_local(calling_player:Node, func_name:String, args:Array = []):
	for id in [1] + players.keys():
		if id != get_tree().get_network_unique_id():
			var arglist = [id, func_name] + args
			calling_player.callv("rpc_id", arglist)

# Send an Unreliable RPC to every player in this game. Called by player object.
func rpc_local_unreliable(calling_player:Node, func_name:String, args:Array = []):
	for id in [1] + players.keys():
		if id != get_tree().get_network_unique_id():
			var arglist = [id, func_name] + args
			calling_player.callv("rpc_unreliable_id", arglist)

# Time scoring
func _process(delta):
	if is_network_master():
		# score times
		for id in players.keys():
			if not id in players_finished and timer_started == true:
				player_times[id] += delta
		rpc_local(self, "update_times", [player_times])
		
		# end level timer
		if is_end_timer_active:
			end_timer_value -= delta
			if end_timer_value <= 0:
				start_next_level()
		rpc_local(self, "update_end_timer", [is_end_timer_active, end_timer_value])
	
	# UI
	UI.update_scores(players, player_times)
	
	UI.update_time(end_timer_value/END_TIMER_DUR)
	if is_end_timer_active:
		UI.show_timer()
	else:
		UI.hide_timer()
	
	
	# cheaty skip
#	if Input.is_action_just_pressed("ui_page_up"):
#		on_win()

puppet func update_times(new_times):
	player_times = new_times

puppet func update_end_timer(new_bool, new_value):
	is_end_timer_active = new_bool
	end_timer_value = new_value

func init(game_id:String, player_list:Dictionary, level_series:Array):
	name = game_id
	players = player_list
	level_sequence = level_series
	cur_level = -1

	# init scores to 0
	for id in players.keys():
		player_times[id] = 0.0

func start_next_level():
	# Server Only
	if !is_network_master(): return
	
	if cur_level < level_sequence.size() - 1:
		goto_next_level()
		rpc_local(self, "goto_next_level")
		players_loaded = []
		
		# reset level end timer
		is_end_timer_active = false
		end_timer_value = END_TIMER_DUR
		rpc_local(self, "update_end_timer", [is_end_timer_active, end_timer_value])
		
	else:
		print("GAME OVER, deleting")
		rpc_local(self, "goto_winscreen")
		self.queue_free()

puppet func goto_winscreen():
	clear_level()
	NetworkManager.result_players = players
	NetworkManager.result_scores = player_scores
	NetworkManager.result_times = player_times
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://Scenes/WinScreen.tscn")
	self.queue_free()

puppetsync func goto_next_level():
	clear_level()
	timer_started = false
	yield(get_tree(), "idle_frame") # wait until level is really gone
	yield(get_tree(), "idle_frame") # wait until level is really gone
	cur_level += 1
	UI.update_scores(players, player_scores)
	load_level(level_sequence[cur_level])

func load_level(level_path:String):
	var level = load(level_path).instance()
	add_child(level)
	level.name = "LEVEL"
	level.get_node("SpawnPoint").visible = false
	spawn_point = level.get_node("SpawnPoint").position
	Notifications.notify("Level Loaded")
	camera.update_level_camera()
	camera.set_camera_to_level()
	
	# Tell server we've loaded
	if !is_network_master():
		rpc_id(1, "done_preconfiguring")

# Straight from the docs
master func done_preconfiguring():
	var who = get_tree().get_rpc_sender_id()
	# Here are some checks you can do, for example
	assert(get_tree().is_network_server())
	assert(who in players.keys()) # Exists
	assert(not who in players_loaded) # Was not added yet

	players_loaded.append(who)

	if players_loaded.size() == players.size():
		yield(get_tree().create_timer(1.0), "timeout") # dramatic start
		spawn_players()
		rpc_local(self, "spawn_players")
		timer_started = true

# Remove all existing level data from the game instance
func clear_level():
	players_finished = []
	
	for child in get_children():
		if child.name == "PLAYERS":
			# clear players but keep parent node
			for grand_child in child.get_children():
				grand_child.queue_free()
		elif child.name == "LEVEL":
			child.queue_free()
		else:
			# ignore anythine else
			pass

puppetsync func spawn_players():
	for id in players.keys():
		var player = PLAYER_SCENE.instance()
		player.init(id, spawn_point)
		player.setName(players[id]["name"])
		player.setColor(players[id]["color"])
		#player.setTrail(get_gradient(players[id]["trail"], players[id]["trail_color"]))
		player.set_network_master(id)
		$PLAYERS.add_child(player)
		if id == get_tree().get_network_unique_id():
			camera.focus = player
			camera.current = true

#func get_gradient(type:String, col:Color=Color.white) -> Gradient:
#	match type:
#		'rainbow':
#			var grad = load("res://Resources/RainbowGradient.tres")
#			return grad
#		'normal', _:
#			var grad = load("res://Resources/TrailGradient.tres").duplicate(true)
#			for i in range(grad.get_point_count()):
#				grad.set_color(i, Color(col.r, col.g, col.b, grad.get_color(i).a))
#			return grad

#I dont think we use this function
func create_player(name : String = "UNNAMED", color : Color = Color.white):
	var newPlayer = PLAYER_SCENE.instance()
	newPlayer.setName(name)
	newPlayer.setColor(color)
	$PLAYERS.add_child(newPlayer)

func on_player_left(other_player_id):
	# Only called by server
	if is_network_master():
		erase_player(other_player_id)
		if players.size() == 0:
			print("Game Empty, deleting")
			self.queue_free()
			return
		for id in players.keys():
			rpc_id(id, "erase_player", other_player_id)

remotesync func erase_player(other_player_id):
	if players.has(other_player_id):
		print("PLAYER %s DISCONNECTED IN-GAME" % other_player_id)
		Notifications.notify("%s (%s) has disconnected." % [players[other_player_id]["name"], other_player_id])
# warning-ignore:return_value_discarded
		players.erase(other_player_id)
		player_times.erase(other_player_id)
		players_finished.erase(other_player_id)
		UI.update_scores(players, player_scores)
	var player = $PLAYERS.get_node_or_null(str(other_player_id))
	if player != null:
		player.queue_free()

#remotesync func update_timer(amount):
#	UI.update_timer(amount)


func on_win():
	rpc_id(1, "register_completion")
	camera.focus = null
	camera.set_camera_to_level()

master func register_completion():
	var player_id = get_tree().get_rpc_sender_id()
	if player_id in players_finished: return
	players_finished.append(player_id)
	rpc_local(self, "update_completion", [players_finished])
	
	# check to start end level timer
	if USE_END_TIMER and !is_end_timer_active:
		is_end_timer_active = true
		end_timer_value = END_TIMER_DUR
		rpc_local(self, "update_end_timer", [is_end_timer_active, end_timer_value])
	
	# check to go to next level
	if players_finished.size() == players.size():
		start_next_level()

puppet func update_completion(new_data):
	players_finished = new_data
