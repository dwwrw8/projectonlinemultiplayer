#Menu.gd
#The main menu of the game, probably.
extends Node2D


func _on_PlayOnline_pressed():
	# randomize color
	NetworkManager.my_data["color"] = Color(randf(),randf(),randf())
	# go to menu
	get_tree().change_scene("res://Scenes/ConnectionMenu.tscn")


func _on_PlayLocal_pressed():
	NetworkManager.startOfflineGame()
	self.queue_free()


var player_name = ""
func _on_LineEdit_text_changed(new_text):
	if !new_text.empty():
		player_name = new_text.substr(0, 12)
	else:
		player_name = "Anonymous"
	NetworkManager.new_player_name(player_name)
