extends ViewportContainer

var GAME_INSTANCE = preload("res://Scenes/GameInstance.tscn")
var OFFLINE_GAME_INSTANCE = preload("res://Scenes/GameInstance-Offline.tscn")

var HOLE_SEQUENCES = {
	"default":[
		"res://Scenes/RealLevels/LevelOne.tscn",
		"res://Scenes/RealLevels/LevelTwo.tscn",
		"res://Scenes/RealLevels/LevelThree.tscn",
		"res://Scenes/RealLevels/LevelFour.tscn",
		"res://Scenes/RealLevels/LevelFive.tscn",
		"res://Scenes/RealLevels/LevelSix.tscn",
		"res://Scenes/RealLevels/LevelSeven.tscn",
		"res://Scenes/RealLevels/LevelEight.tscn",
		"res://Scenes/RealLevels/LevelNine.tscn",
	],
	"offline":[
		"res://Scenes/RealLevels/LevelOne.tscn",
		"res://Scenes/RealLevels/LevelTwo.tscn",
		"res://Scenes/RealLevels/LevelThree.tscn",
		"res://Scenes/RealLevels/LevelFour.tscn",
		"res://Scenes/RealLevels/LevelFive.tscn",
		"res://Scenes/RealLevels/LevelSix.tscn",
		"res://Scenes/RealLevels/LevelSeven-Offline.tscn", # special offline level
		"res://Scenes/RealLevels/LevelEight.tscn",
		"res://Scenes/RealLevels/LevelNine.tscn",
	]
}

# Called when the node enters the scene tree for the first time.
func _ready():
	set_network_master(1)
	rect_size = OS.window_size
	visible = false
	
	# confine the mouse to the window
	Input.mouse_mode = Input.MOUSE_MODE_CONFINED

func remove_all_games():
	for child in get_children():
		child.queue_free()

# Start a new game on the server
func start_game(players : Dictionary):
	# Only called by server
	if not is_network_master():
		return
	
	var hole_sequence = 'default'
	
	var game_id = UUID.NewID()
	srv_start_game(players, game_id, hole_sequence)
	
	for id in players.keys():
		rpc_id(id, "srv_start_game", players, game_id, hole_sequence)
	
	# Tell the server to start the first level
	get_node(game_id).start_next_level()

func start_offline_game(players : Dictionary):
	var hole_sequence = 'offline'
	var game_id = UUID.NewID()
	var game = OFFLINE_GAME_INSTANCE.instance()
	game.init(game_id, players, HOLE_SEQUENCES[hole_sequence])
	self.add_child(game)
	visible = true
	game.start_next_level()


puppetsync func srv_start_game(players : Dictionary, game_id:String, level_sequence: String = "default"):
	if !is_network_master(): get_tree().get_root().get_node("PlayerList").queue_free()
	var game = GAME_INSTANCE.instance()
	game.init(game_id, players, HOLE_SEQUENCES[level_sequence])
	self.add_child(game)
	if !is_network_master():
		visible = true

# Toggle confined mouse
func _input(event):
	if event.is_action_pressed("ui_cancel"):
		if Input.mouse_mode == Input.MOUSE_MODE_CONFINED:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		else:
			Input.mouse_mode = Input.MOUSE_MODE_CONFINED
