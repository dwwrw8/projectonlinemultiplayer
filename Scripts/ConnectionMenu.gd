extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().connect('connected_to_server', self, '_on_connected_to_server')
	# default to server 1
	$VBoxContainer/Server1.pressed = true
	_on_Server1_pressed()

var ip = ""
var port = 0

func _on_ConnectButton_pressed():
	if $VBoxContainer/CustomIP.pressed:
		ip = $IPAddress.text
		port = int($Port.text)

	if ip == "" : ip = NetworkManager.DEFAULT_IP
	if port == 0 : port = NetworkManager.DEFAULT_PORT
	
	NetworkManager.connect_to_server(ip, port)
	#TODO Add loading graphic to connection menu

func _on_connected_to_server():
	get_tree().change_scene("res://Scenes/LobbyList.tscn")

func on_back_button_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")

func enable_input():
	$IPAddress.editable = true
	$Port.editable = true
func disable_input():
	$IPAddress.editable = false
	$Port.editable = false

func _on_Server1_pressed():
	ip = "64.15.81.244"
	port = 26969
	disable_input()

func _on_Server2_pressed():
	ip = "3.15.188.170"
	port = 26969
	disable_input()

func _on_Localhost_pressed():
	ip = "localhost"
	port = 26969
	disable_input()

func _on_CustomIP_pressed():
	ip = $IPAddress.text
	port = $Port.text
	enable_input()
