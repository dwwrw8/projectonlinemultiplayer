extends Area2D

func _on_EndingArea_body_entered(body):
	if body.has_method("on_enter_hole"):
		body.on_enter_hole()
