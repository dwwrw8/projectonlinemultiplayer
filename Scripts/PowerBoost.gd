extends Area2D

export var duration = 30 # seconds

# Applies the power boost to the body
func _on_PowerBoost_body_entered(body):
	if body.has_method("apply_power_boost") and body.is_network_master():
		body.apply_power_boost(duration)
		queue_free()
