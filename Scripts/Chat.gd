extends CanvasLayer

const MESSAGE_TIMEOUT := 10.0 # Seconds

func _input(event) -> void:
	
	# Detect the user pressing enter
	if event.is_action_pressed("chat_send"):
		
		# Start new message
		if not $LineEdit.visible:
			$LineEdit.show()
			$LineEdit.grab_focus()
		
		# Send typed message
		else:
			var message_to_send: String = $LineEdit.get_text()
			if not message_to_send.empty():
				send_message(message_to_send)
				$LineEdit.clear()
			
			# Hide chat box
			$LineEdit.hide()

# Called locally, sends a message to all connected peers
func send_message(message:String) -> void:
	if get_tree().network_peer != null:
		rpc("receive_message", message, NetworkManager.get_my_name())
	else:
		receive_message("ur not online yet bozo", "SYSTEM")

# Called remotely, actually puts a message on the screen
remotesync func receive_message(message:String, sender:String) -> void:
#	Notifications.notify(message)
	# create label to hold message
	var label = Label.new()
	label.autowrap = true
	label.add_constant_override("line_spacing", 0)
	label.text = "%s: %s" % [sender, message]
	
	# add it to the tree
	$MessageList.add_child(label)
	
	# remove after a delay
	yield(get_tree().create_timer(10), "timeout")	
	label.queue_free()
