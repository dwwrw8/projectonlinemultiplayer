extends PanelContainer


func updateData(lobbyData, playerID):
	var my_data = lobbyData[NetworkManager.currentLobby]["players"][playerID]
	$VBoxContainer/Label.text = my_data["name"]
	$VBoxContainer/PanelContainer/Ball.modulate = my_data["color"]
