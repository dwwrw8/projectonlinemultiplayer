extends RigidBody2D

onready var GAME = $"../.." 
onready var Nametag = $DisplayHelper/Nametag

var playerName = "Nonymous"
var spawnPoint = Vector2.ZERO
var respawn_next_frame := false

var canFire = true

# An array to track the power of the player over the last few frames
var velocityTrackArray = []
const velocityTrackLen = 7 # number of past velocities to store
const VELOCITY_THRESHHOLD = 50.0 # max speed that counts as stopped
var isStopped = false

var grounded_time = 0.0 # seconds we've been on the ground
var GROUNDED_THRESHHOLD = 0.3 # seconds we have to be on the ground to shoot
var isGrounded = false

# Potential TODO:
# Limit max hit strength
# Adjust line color by hit strength
# Prevent constant movement (time or velocity check)
# Change color to show when you can move

# Power boost variables
var powerBoost = 2.2 #power multiplier for power boost
onready var powerBoostOn = false #bool for if boost is on or off
var powerBoostTime = 60 #the amount of time the powerup will last in seconds
var timeElapsed = 0 #the amount of time elapsed while holding powerup

# Grab a reference to our aim line
onready var aimLine = $AimLine

# The max distance the aimLine can go
var maxPower = 360
var powerScale = 2.3

export(Gradient) var powerGradient

# Called when the node enters the scene tree for the first time.
func _ready():
	# This makes the aim line's points global, rather than relative
	# to the player position. Prevents annoying math.
	aimLine.set_as_toplevel(true)
	
	# Filling velocityTrackArray with arbitrary values
	velocityTrackArray.resize(velocityTrackLen)
	velocityTrackArray.fill(100)
	Nametag.text = playerName


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	# update visuals
	$Sprite/animatedFlames.visible = powerBoostOn
	# rotate based on movement
	var flame_dir = -linear_velocity + Vector2.UP * 20
#	$Sprite/animatedFlames.global_rotation = flame_dir.angle() + PI/2.0
	$Sprite/animatedFlames.global_rotation = lerp_angle(
		$Sprite/animatedFlames.global_rotation,
		flame_dir.angle() + PI/2.0,
		0.5
	)
	
	# power boost timer
	if(powerBoostOn == true):
		if(timeElapsed < powerBoostTime):
			timeElapsed += delta
		else:
			powerBoostOn = false
			timeElapsed = 0
	
	
	if is_network_master():
		
		# respawn if we died
		if respawn_next_frame:
			self.linear_velocity = Vector2.ZERO
			self.position = spawnPoint + Vector2(randf(), randf())
			respawn_next_frame = false
		
		# The vector between mouse position and player position
		var difference = get_global_mouse_position() - self.global_position
		var direction = difference.normalized()
		var power = clamp(difference.length(), 0, maxPower)
		
		aimLine.points[0] = self.global_position
		aimLine.points[1] = self.global_position + direction * power
		
		aimLine.default_color = powerGradient.interpolate(power/maxPower)
		
		# Tracking the last 5 velocities
		velocityTrackArray.pop_front()
		velocityTrackArray.append(self.linear_velocity.length())
		
		var ground_list = $GroundCheckArea.get_overlapping_bodies()
		if ground_list != null and len(ground_list) > 1:
			# we're touching something
			grounded_time += delta
		else:
			grounded_time = 0
		
		isGrounded = grounded_time >= GROUNDED_THRESHHOLD
		
		# If ball is not moving then update the AimLine and set shotCapable to true
		isStopped = true
		for val in velocityTrackArray:
			if val > VELOCITY_THRESHHOLD:
				isStopped = false
				break
		
		if isStopped and isGrounded:
			aimLine.default_color = powerGradient.interpolate(power/maxPower)
		else: # Otherwise set AimLine color to blue
			aimLine.default_color = "6680ff"
	else:
		aimLine.hide()


# Called every physics update. Use this for physics-related calculations.
func _physics_process(_delta):
	if is_network_master():
		# Check if the player is trying to move (custom action)
		if (Input.is_action_just_pressed("shoot") and canFire and isGrounded and isStopped):
			# Figure out the vector between the ball and the mouse
			var difference = aimLine.points[1] - aimLine.points[0] # Using the points of the aimline to keep power consistent
			
			var power
			if(powerBoostOn == true):
				power = difference * powerScale * powerBoost
			else:
				power = difference * powerScale 
			# Apply a force (do the move)
			self.apply_central_impulse(power)
#		GAME.rpc_local_unreliable(self, "update_state", [global_position, linear_velocity, global_rotation])
#	else:
#		if goal_position != null:
#			global_position = goal_position
#			linear_velocity = goal_velocity
#			global_rotation = goal_rotation
#			goal_position = null
#			goal_velocity = null
#			goal_rotation = null

#var send_next_state = false
func _integrate_forces(state):
	if is_network_master():# and send_next_state:
#		Notifications.notify("sending state")
#		send_next_state = false
		GAME.rpc_local_unreliable(self, "update_state", [state.transform, state.linear_velocity, state.angular_velocity])
	if !is_network_master() and goal_transform != null:
#		Notifications.notify("updateing state")
		state.transform = goal_transform
		state.linear_velocity = goal_velocity
		state.angular_velocity = goal_angular_vel
		goal_transform = null
		goal_velocity = null
		goal_angular_vel = null

# Stores latest data from the network until the next physics frame can process it
#var goal_position = null
#var goal_velocity = null
#var goal_rotation = null
#puppet func update_state(new_position:Vector2, new_velocity:Vector2, new_rotation):
#	goal_position = new_position
#	goal_velocity = new_velocity
#	goal_rotation = new_rotation
var goal_transform = null
var goal_velocity = null
var goal_angular_vel = null
puppet func update_state(new_transform, new_velocity:Vector2, new_angular_vel):
	goal_transform = new_transform
	goal_velocity = new_velocity
	goal_angular_vel = new_angular_vel

func init(id:int, spawn_point:Vector2):
	name = str(id)
	position = spawn_point + Vector2(randf(), randf())
	spawnPoint = spawn_point

func setName(newName : String) -> void:
	playerName = newName

func setColor(color : Color) -> void:
	$Sprite.modulate = color

#func setTrail(grad : Gradient) -> void:
	#var Trail = grad

var has_entered_hole = false
func on_enter_hole():
	if is_network_master():
		if has_entered_hole: return
		has_entered_hole = true
		GAME.on_win()
		canFire = false
		self.hide()
		aimLine.hide()
		#trail.hide()
		#trajline.hide()
		Nametag.hide()
		self.set_collision_mask_bit(0, false) # no collision
		self.set_collision_layer_bit(0, false) # no collision
		GAME.rpc_local(self, "pup_hide_self")
		#GAME.camera.set_camera_to_level()

puppet func pup_hide_self():
	self.hide()
	aimLine.hide()
	#trail.hide()
	#trajline.hide()
	Nametag.hide()
	self.set_collision_mask_bit(0, false)
	self.set_collision_layer_bit(0, false) # no collision

# Applies the power boost, and returns a bool to control whether to delete the powerup
remotesync func apply_power_boost(duration:float = 30):
	powerBoostOn = true
	powerBoostTime = duration
	if is_network_master():
		GAME.rpc_local(self, "apply_power_boost", [duration])

func on_enter_killplane():
	respawn_next_frame = true
