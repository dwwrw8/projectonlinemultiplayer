extends Node2D


onready var playerCamera = $Player/PlayerCamera
onready var levelCamera = $LevelCamera


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called on keypress 'z'. Swaps camera between PlayerCamera and LevelCamera
func _unhandled_input(event):
	# Checks if player hits 'Z' on keyboard
	if event.is_action_pressed("toggle_camera"):
		# Check if camera is on the player
		if playerCamera.is_current():
			levelCamera.current = true
		# Check if camera is on the level
		elif levelCamera.is_current():
			playerCamera.current = true
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
