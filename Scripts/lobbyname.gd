extends PanelContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal lobby_name_entered

# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/LineEdit.grab_focus()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LineEdit_text_entered(new_text):
	emit_signal("lobby_name_entered", new_text.substr(0, 25))

func _on_ConfirmButton_pressed():
	emit_signal("lobby_name_entered", $VBoxContainer/LineEdit.text.substr(0, 25))


func _on_LineEdit_text_changed(new_text):
	$VBoxContainer/HBoxContainer/ConfirmButton.disabled = len(new_text) == 0

func _on_CancelButton_pressed():
	self.queue_free()
