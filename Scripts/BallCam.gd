extends Camera2D

var focus = null

# Placeholder for levelCamera variable
var levelCamera = null


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if focus != null and is_instance_valid(focus):
		self.position = focus.position


# runs when level is loaded, sets levelCamera variable for the level
func update_level_camera():
#	self.current = true
	levelCamera = get_node_or_null("../LEVEL/LevelCamera")


# "runs" when ball enters hole, swaps camera to level, doesn't work
func set_camera_to_level():
	if levelCamera != null:
		levelCamera.current = true


# Called on keypress 'z'. Swaps camera between PlayerCamera and LevelCamera
func _unhandled_input(event):
	# Checks if player hits 'Z' on keyboard
	if event.is_action_pressed("toggle_camera"):
		# Check if camera is on the player
		if self.is_current():
			levelCamera.current = true
		# Check if camera is on the level
		elif levelCamera.is_current():
			self.current = true
	pass

