extends Area2D

var isActive = false

func _on_EndingArea_body_entered(body):
	# ensure this is the active area
	if isActive == true:
		# normal ball logic
		if body.has_method("on_enter_hole"):
			body.on_enter_hole()
			# ensure only the owning player triggers a move
#			if body.is_network_master():
			get_parent().request_randomize_end_area()

func activate():
	$Flag.visible = true
	isActive = true

func deactivate():
	$Flag.visible = false
	isActive = false
