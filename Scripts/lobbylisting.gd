extends HBoxContainer

signal JoinButton_pressed(lobby_id)

var my_id

func updateData(lobbyData, lobbyID):
	my_id = lobbyID
	var my_data = lobbyData[lobbyID]
	#$HBoxContainer/ID.text = name
	$Name.text = lobbyData[lobbyID]["name"]
	$NumPlayers.text = "Players: " + str(my_data["players"].size()) + "/" + str(117)
	if my_data["players"].size() >= 117:
		$Button.disabled = true

func _on_join_button_pressed():
	emit_signal("JoinButton_pressed", my_id)
