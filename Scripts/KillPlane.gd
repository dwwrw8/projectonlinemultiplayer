extends Area2D

func _on_KillPlane_body_entered(body):
	if body.has_method("on_enter_killplane"):
		body.on_enter_killplane()
