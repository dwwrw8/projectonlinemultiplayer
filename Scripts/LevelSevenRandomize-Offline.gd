extends Node2D

onready var endingAreas = [
	$EndingAreaOne,
	$EndingAreaTwo,
	$EndingAreaThree,
	$EndingAreaFour,
	$EndingAreaFive,
	$EndingAreaSix
]

onready var GAME = get_parent()

var curArea = 0
func _ready():
#	if is_network_master():
		randomize_end_area()

func request_randomize_end_area():
	randomize_end_area()

mastersync func randomize_end_area():
#	if is_network_master():
		endingAreas[curArea].deactivate()
		
		# ensure it goes to a new position
		var prev = curArea
		while prev == curArea: curArea = randi() % len(endingAreas)
		
		endingAreas[curArea].activate()
#		GAME.rpc_local(self, "set_active_area", [curArea])

#puppet func set_active_area(index:int) -> void:
#	endingAreas[curArea].deactivate()
#	curArea = index
#	endingAreas[curArea].activate()

# update after a timer in case the initial rpc is too fast
#func _on_EndAreaUpdateTimer_timeout():
#	if is_network_master():
#		GAME.rpc_local(self, "set_active_area", [curArea])
